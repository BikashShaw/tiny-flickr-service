package com.tiny.flickr;

import com.tiny.flickr.properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class TinyFlickrApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TinyFlickrApplication.class, args);
	}

}
