package com.tiny.flickr.properties;

public enum ImageType {
    PHOTO,
    THUMBNAIL
}
