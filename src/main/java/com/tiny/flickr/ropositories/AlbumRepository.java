package com.tiny.flickr.ropositories;

import com.tiny.flickr.entities.Album;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlbumRepository extends JpaRepository<Album, Integer> {
    List<Album> findByUserId(int id);
}
