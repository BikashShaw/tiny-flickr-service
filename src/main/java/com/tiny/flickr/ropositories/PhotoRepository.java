package com.tiny.flickr.ropositories;

import com.tiny.flickr.entities.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface PhotoRepository extends JpaRepository<Photo, Integer> {
    Photo findTopByAlbumIdOrderByIdDesc(int albumId);

    List<Photo> findByAlbumIdOrderByIdDesc(int albumId);

    @Transactional
    void deleteByAlbumId(int albumId);

    Photo findById(int id);

}
