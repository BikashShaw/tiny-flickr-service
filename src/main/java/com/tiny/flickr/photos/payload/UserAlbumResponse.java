package com.tiny.flickr.photos.payload;

import com.tiny.flickr.entities.Album;
import com.tiny.flickr.entities.Photo;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class UserAlbumResponse {
    private Album album;
    private String thumbnailUrl;
    public static final String DEFAULT = "default";
    private static final String DEFAULT_THUMBNAIL = "/photos/api/download/150/photo/";


    public UserAlbumResponse(Album album, Photo photo) {
        this.album = album;

        this.thumbnailUrl = photo != null ? photo.getThumbnailUrl() :
                ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_THUMBNAIL)
                        .path(DEFAULT)
                        .toUriString();
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
