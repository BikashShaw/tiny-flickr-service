package com.tiny.flickr.photos.payload;

public class UploadFileResponse {
    private int id;
    private int albumId;
    private String thumbnailUrl;
    private String fileName;
    private String url;
    private String title;
    private String fileType;
    private long size;

    public UploadFileResponse(int id, String fileName, int albumId, String url, String thumbnailUrl,
                              String title, String contentType, long size) {
        this.id = id;
        this.fileName = fileName;
        this.albumId = albumId;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
        this.title = title;
        this.fileType = contentType;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }
}