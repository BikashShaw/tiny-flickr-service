package com.tiny.flickr.services;

import com.tiny.flickr.entities.Photo;
import com.tiny.flickr.exceptions.FileStorageException;
import com.tiny.flickr.properties.FileStorageProperties;
import com.tiny.flickr.properties.ImageType;
import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

@Service
public class FileStorageService {

    private final Path photoStorageLocation;
    private final Path photoThumbnailStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.photoStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        this.photoThumbnailStorageLocation = Paths.get(fileStorageProperties.getThumbnailDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.photoStorageLocation);
            Files.createDirectories(this.photoThumbnailStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileName = UUID.randomUUID().toString().replace("-", "");

        try {
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.photoStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            //Create thumbnail
            Path thumbnailTargetLocation = this.photoThumbnailStorageLocation.resolve(fileName);
            createThumbnail(file.getInputStream(), thumbnailTargetLocation.toFile());

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public void createThumbnail(InputStream inputStream, File outputLocation) throws IOException {

        BufferedImage thumbImg = Scalr.resize(ImageIO.read(inputStream), Scalr.Method.QUALITY, Scalr.Mode.AUTOMATIC,
                150,
                150, Scalr.OP_ANTIALIAS);

        ImageIO.write(thumbImg, "PNG", outputLocation);
    }

    public Resource loadFileAsResource(String fileName, ImageType imageType) {
        try {
            Path filePath;
            switch (imageType) {
                case THUMBNAIL:
                    filePath = this.photoThumbnailStorageLocation.resolve(fileName).normalize();
                    break;
                default:
                    filePath = this.photoStorageLocation.resolve(fileName).normalize();
            }

            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileStorageException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File not found " + fileName, ex);
        }
    }
    public boolean deletePhoto(Photo photo) {
        String fileName = photo.getUrl().substring(photo.getUrl().lastIndexOf('/') + 1);
        Path targetLocation = this.photoStorageLocation.resolve(fileName);
        Path thumbnailTargetLocation = this.photoThumbnailStorageLocation.resolve(fileName);
        return FileUtils.deleteQuietly(targetLocation.toFile()) && FileUtils.deleteQuietly(thumbnailTargetLocation.toFile());
    }
    public long deletePhotos(List<Photo> photoList) {
        return photoList.stream().map(this::deletePhoto).filter(deleted -> deleted).count();
    }
}
