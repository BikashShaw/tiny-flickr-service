package com.tiny.flickr.controllers;

import com.tiny.flickr.entities.User;
import com.tiny.flickr.exceptions.UserAlreadyExistsException;
import com.tiny.flickr.exceptions.UserNotFoundException;
import com.tiny.flickr.ropositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("users/api")
public class UsersController {

    private final UserRepository userRepository;

    @Autowired
    public UsersController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping(value = "/create")
    public User postCustomer(@RequestBody User user) {
        if(!userRepository.findByUsername(user.getUsername()).isEmpty()) {
            throw new UserAlreadyExistsException("User already exists: " + user.getUsername());
        }
        user.getAddress().setUser(user);
        return userRepository.save(user);
    }

    @PostMapping(value = "/create/all")
    public List<User> postCustomer(@RequestBody List<User> users) {
        for (User user : users) {
            user.getAddress().setUser(user);
        }
        return userRepository.save(users);
    }

    @GetMapping(value = "/user/{username}")
    public User getUsername(@PathVariable("username") String username) {
        List<User> userList = userRepository.findByUsername(username);
        if(userList.isEmpty()) {
            throw new UserNotFoundException("User not found: " + username);
        }
        return userList.get(0);
    }
}