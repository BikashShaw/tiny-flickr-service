package com.tiny.flickr.controllers;

import com.tiny.flickr.entities.Photo;
import com.tiny.flickr.photos.payload.UploadFileResponse;
import com.tiny.flickr.properties.ImageType;
import com.tiny.flickr.ropositories.PhotoRepository;
import com.tiny.flickr.services.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("photos/api")
public class PhotosController {

    private static final Logger logger = LoggerFactory.getLogger(PhotosController.class);

    private final FileStorageService fileStorageService;

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotosController(FileStorageService fileStorageService, PhotoRepository photoRepository) {
        this.fileStorageService = fileStorageService;
        this.photoRepository = photoRepository;
    }

    @PostMapping("/photo/{albumId}/upload")
    public UploadFileResponse uploadFile(@PathVariable("albumId") int albumId, @RequestParam("file") MultipartFile file, @RequestParam("title") String title) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/photos/api/download/photo/")
                .path(fileName)
                .toUriString();

        String fileDownloadThumbnailUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/photos/api/download/150/photo/")
                .path(fileName)
                .toUriString();


        Photo photo = new Photo();
        photo.setUrl(fileDownloadUri);
        photo.setThumbnailUrl(fileDownloadThumbnailUri);
        photo.setAlbumId(albumId);
        photo.setTitle(title);

        Photo savedPhoto = photoRepository.save(photo);

        return new UploadFileResponse(savedPhoto.getId(), fileName, albumId, fileDownloadUri,fileDownloadThumbnailUri,
                title, file.getContentType(), file.getSize());
    }

    @PostMapping("/photos/{albumId}/upload")
    public List<UploadFileResponse> uploadMultipleFiles(@PathVariable("albumId") int albumId, @RequestParam("files") MultipartFile[] files, @RequestParam("title") String title) {
        return Arrays.stream(files)
                .map(file -> uploadFile(albumId, file, title))
                .collect(Collectors.toList());
    }

    @GetMapping("/download/photo/{fileName:.+}")
    public ResponseEntity<Resource> downloadPhoto(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName, ImageType.PHOTO);
        return getResourceResponseEntity(request, resource);

    }

    @GetMapping("/download/150/photo/{fileName:.+}")
    public ResponseEntity<Resource> downloadPhotoThumbnail(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = fileStorageService.loadFileAsResource(fileName, ImageType.THUMBNAIL);
        return getResourceResponseEntity(request, resource);
    }

    @GetMapping("/photos/{albumId}")
    public List<Photo> getPhotosByAlbumId(@PathVariable("albumId") int albumId) {
        return photoRepository.findByAlbumIdOrderByIdDesc(albumId);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable("id") int id) {
        Photo photo = photoRepository.findById(id);
        fileStorageService.deletePhoto(photo);
        photoRepository.delete(id);

        return new ResponseEntity<>("Photo has been deleted!", HttpStatus.OK);
    }

    private ResponseEntity<Resource> getResourceResponseEntity(HttpServletRequest request, Resource resource) {
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
