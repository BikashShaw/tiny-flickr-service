package com.tiny.flickr.controllers;

import com.tiny.flickr.entities.Album;
import com.tiny.flickr.entities.Photo;
import com.tiny.flickr.entities.User;
import com.tiny.flickr.exceptions.UserNotFoundException;
import com.tiny.flickr.photos.payload.UserAlbumResponse;
import com.tiny.flickr.ropositories.AlbumRepository;
import com.tiny.flickr.ropositories.PhotoRepository;
import com.tiny.flickr.ropositories.UserRepository;
import com.tiny.flickr.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("albums/api")
public class AlbumsController {

    private final UserRepository userRepository;

    private final AlbumRepository albumRepository;

    private final PhotoRepository photoRepository;
    private final FileStorageService fileStorageService;

    @Autowired
    public AlbumsController(UserRepository userRepository, AlbumRepository albumRepository, PhotoRepository photoRepository, FileStorageService fileStorageService) {
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.photoRepository = photoRepository;
        this.fileStorageService = fileStorageService;
    }

    @PostMapping(value = "/create/{username}/{title}")
    public UserAlbumResponse postCreateAlbum(@PathVariable("username") String username, @PathVariable("title") String title) {
        List<User> userList = userRepository.findByUsername(username);
        if (userList.isEmpty()) {
            throw new UserNotFoundException("User not found: " + username);
        }
        Album album = new Album();
        album.setTitle(title);
        album.setUserId(userList.get(0).getId());
        albumRepository.save(album);
        return createUserAlbumResponse(album);
    }

    @GetMapping(value = "/albums")
    public List<Album> getAlbums() {
        return albumRepository.findAll();
    }

    @GetMapping(value = "/albums/{username}")
    public List<UserAlbumResponse> getAlbumsByUsername(@PathVariable("username") String username) {
        List<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UserNotFoundException("User not found: " + username);
        }
        List<Album> albums = albumRepository.findByUserId(user.get(0).getId());

        return albums.stream().map(this::createUserAlbumResponse).collect(Collectors.toList());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable("id") int id) {
        List<Photo> photos = photoRepository.findByAlbumIdOrderByIdDesc(id);

        fileStorageService.deletePhotos(photos);

        photoRepository.deleteByAlbumId(id);

        albumRepository.delete(id);

        return new ResponseEntity<>("Album has been deleted!", HttpStatus.OK);
    }

    private UserAlbumResponse createUserAlbumResponse(Album album) {
        Photo photo = this.photoRepository.findTopByAlbumIdOrderByIdDesc(album.getId());

        return new UserAlbumResponse(album, photo);
    }
}
